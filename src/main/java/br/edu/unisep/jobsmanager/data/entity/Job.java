package br.edu.unisep.jobsmanager.data.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "job")
public class Job {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_job")
    private Integer id;

    @Column(name = "title")
    private String title;

    @Column(name = "description")
    private String description;

    @Column(name = "seniority")
    private String seniority;

    @Column(name = "salary")
    private Double salary;

    @OneToOne
    @JoinColumn(name = "id_company")
    private Company company;
}
