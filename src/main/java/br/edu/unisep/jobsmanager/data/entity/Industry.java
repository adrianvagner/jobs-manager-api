package br.edu.unisep.jobsmanager.data.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "industry")
public class Industry {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_industry")
    private Integer id;

    @Column(name = "name")
    private String name;
}
