package br.edu.unisep.jobsmanager.controller;

import br.edu.unisep.jobsmanager.domain.dto.industry.IndustryDto;
import br.edu.unisep.jobsmanager.domain.usecase.industry.FindAllIndustriesUseCase;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/industry")
@AllArgsConstructor
public class IndustryController {

    private FindAllIndustriesUseCase findAllUseCase;

    @GetMapping
    public ResponseEntity<List<IndustryDto>> findAll() {
        List<IndustryDto> industries = findAllUseCase.execute();
        return ResponseEntity.ok(industries);
    }
}
